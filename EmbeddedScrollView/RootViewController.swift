//
//  RootViewController.swift
//  EmbeddedScrollView
//
//  Created by Lorenzo Lewis on 1/28/21.
//

import UIKit

class RootViewController: UIViewController {
    
    private var parentScrollView: UIScrollView = {
        let view = UIScrollView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .systemGreen
        
        return view
    }()
    
    private var childScrollView: UIScrollView = {
        let view = UIScrollView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .systemPink
        
        return view
    }()
    
    private var stackView: UIStackView = {
        
        var labels = [UILabel]()
        
        for i in 0...10 {
            let label = UILabel()
            label.text = "Label \(i)"
            label.backgroundColor = .systemBlue
            labels.append(label)
        }
        
        let view = UIStackView(arrangedSubviews: labels)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .systemBlue
        view.spacing = 8
        view.axis = .horizontal
        
        return view
    }()
    
    private var fillerStackView: UIStackView = {
        
        var labels = [UILabel]()
        
        for i in 0...100 {
            let label = UILabel()
            label.text = "Row \(i)"
            labels.append(label)
        }
        
        let view = UIStackView(arrangedSubviews: labels)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.axis = .vertical
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
        
        view.addSubview(parentScrollView)
        
        parentScrollView.addSubview(childScrollView)
        childScrollView.addSubview(stackView)
        parentScrollView.addSubview(fillerStackView)
        
        NSLayoutConstraint.activate([
            parentScrollView.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor, constant: 10),
            parentScrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10),
            parentScrollView.bottomAnchor.constraint(equalTo: view.layoutMarginsGuide.bottomAnchor, constant: -10),
            parentScrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10),
            
            childScrollView.topAnchor.constraint(equalTo: parentScrollView.topAnchor, constant: 10),
            childScrollView.leadingAnchor.constraint(equalTo: parentScrollView.leadingAnchor, constant: 10),
            childScrollView.heightAnchor.constraint(equalTo: stackView.heightAnchor, constant: 20),
            childScrollView.trailingAnchor.constraint(equalTo: parentScrollView.trailingAnchor, constant: -10),
            childScrollView.widthAnchor.constraint(equalTo: parentScrollView.widthAnchor, constant: -20),
            
            stackView.topAnchor.constraint(equalTo: childScrollView.topAnchor, constant: 10),
            stackView.leadingAnchor.constraint(equalTo: childScrollView.leadingAnchor, constant: 10),
            stackView.bottomAnchor.constraint(equalTo: childScrollView.bottomAnchor, constant: -10),
            stackView.trailingAnchor.constraint(equalTo: childScrollView.trailingAnchor, constant: -10),
            
            fillerStackView.topAnchor.constraint(equalTo: childScrollView.bottomAnchor, constant: 10),
            fillerStackView.leadingAnchor.constraint(equalTo: parentScrollView.leadingAnchor, constant: 10),
            fillerStackView.bottomAnchor.constraint(equalTo: parentScrollView.bottomAnchor, constant: -10),
            fillerStackView.trailingAnchor.constraint(equalTo: parentScrollView.trailingAnchor, constant: -10)
        ])
    }
}
